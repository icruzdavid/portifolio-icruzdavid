# portifolio-icruzdavid
Projeto para comprovação de conhecimento nas tecnologias e experiência.

## Tarefas

O controle das tarefas desse projeto será realizado no GitHub.

## Icones

:package: nova funcionalidade
:new: atualização
:white_check_mark: bug corrigido 
:x: bug detectado
:page_facing_up: release

## Controle de Versão

Pasta master é a que recebe código funcinando e otimizado.
Pasta staging é a que recebe código em desenvolvimento.
